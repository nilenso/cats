ALTER TABLE applications
DROP COLUMN updated_at,
ALTER COLUMN created_at DROP NOT NULL;
