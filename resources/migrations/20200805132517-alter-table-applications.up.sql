ALTER TABLE applications
ADD COLUMN updated_at TIMESTAMPTZ,
ALTER COLUMN created_at SET NOT NULL,
ADD CONSTRAINT updated_at_gt_than_or_eq_to_created_at
CHECK (updated_at >= created_at);
