CREATE TABLE IF NOT EXISTS applications (
id               UUID PRIMARY KEY NOT NULL,
applicant_name   TEXT NOT NULL,
email            TEXT NOT NULL,
phone            TEXT NOT NULL,
created_at       TIMESTAMPTZ,
description      TEXT,
resume           BYTEA,
resume_filename TEXT
CONSTRAINT resume_filename_iff_resume
CHECK ((resume_filename IS NULL and resume IS NULL )
OR (resume_filename IS NOT NULL and resume IS NOT NULL ))
);
