(ns cats.routes
  (:require [bidi.bidi :as bd]
            [bidi.ring :as bdr]
            [cats.handler :as ch]
            [ring.middleware.defaults :as rmd]
            [ring.middleware.json :as rmj]
            [ring.middleware.content-type :as rmct]
            [ring.util.response :as rur]))


;; Route for add application
;; - add, application
;; BY hiring owner -> session protected

;; GET /applications    -> list (paginated)
;; POST /applications   -> add
;; GET /applications/id -> application
;; PUT /applications/id
;; DEL /applications/id


(def routes
  ["/" {"applications" {:post ch/add-one-application
                        :get ch/get-applications
                        ["/" :id] {:put ch/update-one-application
                                   :get ch/get-one-application}
                        ["/" :id "/resume"] {:get ch/get-resume}}
        :get (constantly
              (rur/response "Meow."))

        true (constantly
              (rur/not-found "Not found."))}])


(def app
  (-> routes
      bdr/make-handler
      rmj/wrap-json-response
      ;; add mime-types not defined in ring.util.mime-type/default-mime-types
      (rmct/wrap-content-type {:mime-types {"org" "text/plain"
                                            "md" "text/plain"
                                            "docx" "application/msword"}})
      (rmd/wrap-defaults {:params {:urlencoded true ; use when passing query params
                                   :keywordize true
                                   :multipart true}})))


(comment
  (bd/match-route routes
                  "/applications/42"
                  :request-method :get)

  (bd/match-route routes
                  "/applications/42/resume"
                  :request-method :get)
  )
