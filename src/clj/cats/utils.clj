(ns cats.utils
  (:require [clojure.java.io :as jio]))


(defn read-file->bytearray
  "Use this to read a resume document, that we can put into a DB.
  For convenience, read the sample file: resource/test/resume.pdf"
  [file-path]
  (when file-path
    (with-open [in (jio/input-stream
                    (jio/file file-path))
                out (java.io.ByteArrayOutputStream.)]
      (jio/copy in out)
      (.toByteArray out))))


(defn uuid
  "When called with no arguments, return a random UUID.
  When passed a string, convert it to a UUID if it conforms to the UUID pattern,
  else return nil."
  ([]
   (java.util.UUID/randomUUID))
  ([string]
   ;; Guard for strings, so we can catch the proper exception if the string
   ;; fails to parse as a UUID.
   (when (string? string)
     (try (java.util.UUID/fromString string)
          (catch IllegalArgumentException e
            (println (ex-message e))
            nil)))))


(defn numeric?
  "Will the string parse as a number?"
  [s]
  (and s (re-matches #"^\d+$" s)))


(defn str->non-neg-int
  "\"Not like this... Not like this.\" - Switch, The Matrix.

  Parse a string as an Integer, only when it parses as a non-negative number.
  Return the fallback value, or nil if none provided."
  ([s]
   (str->non-neg-int s nil))
  ([s fallback]
   (if (numeric? s)
     (Integer/parseInt s)
     fallback)))
