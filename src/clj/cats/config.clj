(ns cats.config
  "Mount states, and system configurations for the app."
  (:require [aero.core :as aero]
            [clojure.java.io :as jio]
            [mount.core :as mount :refer [defstate]]))


(def known-env?
  #{:dev :test :staging :prod})


(defn get-legal-profile-or-die!
  []
  (let [env (-> (System/getenv)
                (get "CATS_ENV" :dev)
                keyword)]
    (assert (known-env? env)
            (format "Bad CATS_ENV! Got %s, expect one of %s. Dying!"
                    (name env) (mapv name known-env?)))
    (println "About to start CATS with profile: " env)
    env))


(defstate profile
  "Bootstrap :dev profile by default, so the default behaviour does not
mess with :test or, heaven forfend, :prod. This means we must explicitly
start mount with the :prod profile at the main entry point, or the :test
profile in tests."
  :start (get-legal-profile-or-die!)
  :stop nil)


(defn read-legal-config-or-die!
  [profile]
  (let [config (aero/read-config (jio/resource "config.edn")
                                 {:profile profile})]
    (assert (not (contains? (:secrets config)
                            :aero/missing-include)))
    config))


(defstate config
  "The runtime config map for the current profile. Presumes secrets.edn
is reachable via the appropriate path variable set in the environment.
Ref. config.edn for which path variable to use."
  :start (read-legal-config-or-die! profile)
  :stop nil)


(comment
  (mount/find-all-states)

  (do (mount/start #'profile)
      (mount/start #'config))
  config
  (mount/stop #'config)

  (do (mount/stop)
      (mount/start)
      {:profile profile
       :config config})

  (do (mount/stop)
      (mount/start-with {#'cats.config/profile :test})
      {:profile profile
       :config config})
  )
