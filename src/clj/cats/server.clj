(ns cats.server
  (:require [cats.config :as cc]
            [cats.routes :as cr]
            [mount.core :refer [defstate]]
            [ring.adapter.jetty :as raj]))


(defn init
  []
  (println "CATS is starting!"))


(defn destroy
  []
  (println "CATS is stopping!"))


(defn start
  [config]
  (raj/run-jetty #'cats.routes/app
                 config))


(defstate config
  "Profile-defined configuration map to use to start the server."
  :start (assoc (:server cc/config)
                :init init
                :destroy destroy)
  :stop nil)


(defstate server
  "The application server state."
  :start (start config)
  :stop (.stop server))
