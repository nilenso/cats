(ns cats.handler
  (:require [cats.db.models :as models]
            [cats.utils :as cu]
            [ring.util.response :as rur]
            [clojure.java.io :as jio]))


(defn handler
  [request]
  {:status 200
   :headers {"Content-Type" "text/html"}
   :body "Meow."})


(defn add-one-application
  [{{:keys [applicant_name email phone description resume]} :params
    :as request}]
  (let [resume-bytes (cu/read-file->bytearray (:tempfile resume))]
    (-> {:id (models/add-application {:applicant_name applicant_name
                                      :email email
                                      :phone phone
                                      :description description
                                      :resume resume-bytes
                                      :resume_filename (:filename resume)})}
        rur/response)))


(defn get-one-application
  [{{:keys [id]} :route-params
    :as request}]
  (->  (models/get-one-application
        (cu/uuid id))
       rur/response))


(defn get-applications
  "Return a map with:
  - `:page` of applications, each matching what get-one-application returns,
  - `:offset` (zero-indexed) and `:limit` values defining the returned :page
  - `:total` number of applications in DB
  - `:next` url, when a next page exists in the DB
  - `:previous` url, when a previous page exists in the DB

  We try to return _up to_ `:limit` no. of applications, where `:limit` is
  constrained to a :min -to- :max range (both inclusive).

  TODO: perhaps return current page number, and total page number too?"
  [{{:keys [offset limit]
     :or {offset "0"
          limit (str (:min models/limit-range))}} :params
    :as request}]

  ;; Behold, this deeply nested horror.
  ;; Reason #498 to want monads, or maybe the thrush combinator... hmm.

  (let [limit (if (empty? limit)
                (:min models/limit-range)
                (cu/str->non-neg-int limit))
        offset (if (empty? offset)
                 0
                 (cu/str->non-neg-int offset))
        make-url (fn get-applications-url
                   [offset limit]
                   (format "applications?offset=%s&limit=%s" offset limit))]
    (cond
      ;; defend against illegal limit
      (not (and limit
                (<= (:min models/limit-range)
                    limit
                    (:max models/limit-range))))
      (rur/bad-request (format "Limit must fall in the range %s, both inclusive."
                               models/limit-range))
      ;; defend against illegal offset
      (not (and offset
                (<= 0 offset)))
      (rur/bad-request (format "Offset must be non-negative."))

      ;; respond successfully only when we can retrieve applications
      :else (let [applications (models/get-applications offset limit)
                  ;; ensure :next offset is always non-negative, and
                  ;; at most points to the last page, but never beyond
                  offset-next (min (+ offset limit)
                                   (max (- (:total applications)
                                           limit)
                                        0))
                  ;; ensure :previous offset is always non-negative,
                  ;; and is always a page behind next offset
                  offset-previous (max (- offset-next limit)
                                       0)]
              (cond
                (empty? applications)
                (rur/not-found "No applications found in DB.")

                (>= offset (:total applications))
                (rur/not-found "Offset is out of bounds.")

                :else (rur/response
                       (assoc applications
                              :offset offset
                              :limit limit
                              :next (make-url offset-next
                                              limit)
                              :previous (make-url offset-previous
                                                    limit))))))))


(defn update-one-application
  [{{:keys [applicant_name email phone description resume]} :params
    {:keys [id]}  :route-params
    :as request}]
  ;; protect model from nil values
  (let [clean-request (->> {:id (cu/uuid id)
                            :applicant_name applicant_name
                            :email email
                            :phone phone
                            :description description
                            :resume (cu/read-file->bytearray
                                     (:tempfile resume))
                            :resume_filename (:filename resume)}
                           (remove (comp nil? val))
                           (into {}))]
    (if-let [updated-application (models/update-application
                                  clean-request)]
      (rur/response updated-application)
      (rur/bad-request "Won't update."))))


(defn get-resume
  [{{:keys [id]} :route-params
    :as request}]
  (let [{resume-bytes :applications/resume
         resume-filename :applications/resume_filename}
        (models/get-resume
         (cu/uuid id))]
    (if resume-bytes
      (-> resume-bytes
          jio/input-stream
          rur/response
          ;; Suggest a file name via the appropriate HTTP header
          ;; https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Disposition
          (rur/header "Content-Disposition"
                      (format "inline; filename=%s"
                              (or resume-filename "resume"))))
      (rur/not-found "Not found."))))


(comment
  (let [sample-application {:params {:applicant_name (str "A Random Person " (rand))
                                     :email "foobar@example.com"
                                     :phone "+9198507654321"
                                     :description "A random candidate added for no reason"}}
        application-id (str (:id (:body (add-one-application sample-application))))]
    [application-id
     (get-one-application {:route-params {:id application-id}})
     (get-resume {:route-params {:id application-id}})
     (update-one-application (update-in sample-application
                                        [:params]
                                        assoc
                                        :id application-id
                                        :email (str "foobar" (rand) "@example.com")
                                        :phone "+911111100000"
                                        :resume {:tempfile "resources-devtest/data/resume.md"
                                                 :filename "resume.md"}))
     (get-resume {:route-params {:id application-id}})])

  (update-in (:body (get-applications
                     {:route-params {:limit "" :offset ""}}))
             [:page] count)
  )
