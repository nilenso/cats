(ns cats.db.migrations
  (:require [cats.config :as cc]
            [cats.db.core :as cdc]
            [migratus.core :as migratus]
            [mount.core :as mount :refer [defstate]]))

(defstate migratus-config
  :start (-> cc/config
             :database
             :migration
             (assoc :db cdc/datasource))
  :stop nil)


(defn migrate!
  []
  (migratus/migrate migratus-config))


(defn rollback!
  []
  (migratus/rollback migratus-config))


(comment
  (migrate!)
  (rollback!)
  )
