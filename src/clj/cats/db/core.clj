(ns cats.db.core
  (:require [cats.config :as cc]
            [mount.core :as mount :refer [defstate]]
            [next.jdbc.connection :as connection])
  (:import com.zaxxer.hikari.HikariDataSource))


(defn connect-db
  []
  (connection/->pool HikariDataSource
                     (-> cc/config :database :connection)))


(defn disconnect-db
  [^HikariDataSource datasource]
  (.close datasource))


(defstate datasource
  "Postgresql datasource, with connection pooling."
  :start (connect-db)
  :stop (disconnect-db datasource))
