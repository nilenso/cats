(ns cats.db.models
  "Store all the things.

  NOTE: We have chosen to store resumes directly in the applications table.

  TODO: How to properly sanitize incoming DB query parameters and column names?
  TODO: How to avoid manual string-bashing, yet write queries cleanly? Use honeysql?
  TODO: How to avoid duplicating schema and/or sprinkling it all over the place?"
  (:require [cats.db.core :as cdc]
            [clojure.string :as cljstr]
            [next.jdbc :as jdbc]
            [next.jdbc.sql :as sql]))


(def applications-table-name
  :applications)

(def applications-table-name-str
  (name applications-table-name))

(def applications-table-columns
  ["id" "applicant_name" "email" "phone" "created_at"
   "description" "resume" "resume_filename" "updated_at"])

(def applications-table-columns-except-resume
  (filter (complement #{"resume"})
          applications-table-columns))

(def limit-range
  "Clients may request applications in this fixed limit range only."
  {:min 10
   :max 100})


(defn- space-separated
  [strs]
  (cljstr/join " " strs))


(defn- comma-separated
  [strs]
  (cljstr/join ", " strs))


(defn- table-clause
  [table-name col-names]
  (format "%s(%s)"
          table-name
          (comma-separated col-names)))


(defn- values-clause
  [col-names]
  (-> (count col-names)
      (repeat "?")
      comma-separated
      (->> (format "values(%s)"))))


(defn- set-clause
  [col-names]
  (->> col-names
       (mapv (fn [col] (format "%s = ?" col)))
       comma-separated
       (format "SET %s")))


(defn add-application
  "Add a single application to the applications table, and return the
  application ID when successful. Application details must be a map of
    {:keys [applicant_name email phone description
            resume resume_filename]}"

  ([application-details]
   (add-application cdc/datasource
                    application-details))
  ([datasource application-details]
   (let [timestamp-now (java.time.LocalDateTime/now)
         application-details (assoc application-details
                                    :id (java.util.UUID/randomUUID)
                                    :created_at timestamp-now
                                    :updated_at timestamp-now)
         record-vals (mapv (fn [k]
                             (get application-details (keyword k)))
                           applications-table-columns)
         query-str (space-separated
                    ["insert into"
                     (table-clause applications-table-name-str
                                   applications-table-columns)
                     (values-clause applications-table-columns)
                     "returning id"])
         jdbc-query-vec (into [query-str] record-vals)]
     (-> (jdbc/execute-one! datasource
                            jdbc-query-vec)
         :applications/id))))


(defn update-application
  "Update application and return whole record (minus resume)."
  ([application-details]
   (update-application cdc/datasource
                       application-details))
  ([datasource application-details]
   (when-let [update-details (not-empty
                              (dissoc application-details
                                      :id :created_at :updated_at))]
     (let [col-val-tuples (mapv (fn [[k v]] [(name k) v])
                                (assoc update-details
                                       :updated_at (java.time.LocalDateTime/now)))
           query-str (space-separated
                      ["update" applications-table-name-str
                       (set-clause (mapv first col-val-tuples))
                       "where id = ?"
                       "returning" (comma-separated
                                    applications-table-columns-except-resume)])
           jdbc-query-vec (concat [query-str]
                                  (mapv second col-val-tuples)
                                  [(:id application-details)])]
       (dissoc (jdbc/execute-one! datasource
                                  jdbc-query-vec)
               :applications/resume)))))


(defn get-one-application
  "Get all data for an application. Exclude resume by default."
  ([id]
   (get-one-application cdc/datasource id))
  ([datasource id]
   (first
    (sql/query datasource
               [(space-separated
                 ["select" (comma-separated
                            applications-table-columns-except-resume)
                  "from" applications-table-name-str
                  "where id = ?"])
                id]))))


(defn get-applications
  "Fetch a :page of applications. Each returned application has the same
  structure as what get-one-application returns.

  We also return :total applications, to let the client make informed choices."
  ([]
   (get-applications cdc/datasource
                     0
                     (:min limit-range)))
  ([offset limit]
   (get-applications cdc/datasource offset limit))
  ([datasource offset limit]
   (let [;; defend against negative :offset
         offset (if (and offset
                         (pos? offset))
                  offset
                  0)
         ;; defend against negative, and against unbounded :limit
         limit (cond
                 (or (nil? limit)
                     (neg? limit)) (:min limit-range)
                 (< 0 limit (:max limit-range)) limit
                 :else (:max limit-range))]
     ;; NOTE: It's possible to use a single query to look up the paged records,
     ;; _and_ to count all records, but that's a performance optimization, which
     ;; we don't need right now. Also it complicates the query a fair bit.
     ;;
     ;; We count-and-fetch in a transaction for Repeated Read consistency, under
     ;; Postgres's MVCC model. Further, we can tell PG:
     ;;
     ;; - We are performing only read operations here, and
     ;; - "serializable" isolation is fine since our DB is just one box. The
     ;;   manual warns us to consider locking if we ever operate in hot standby.
     ;;
     ;; cf. https://www.postgresql.org/docs/current/applevel-consistency.html
     ;; cf. https://cljdoc.org/d/seancorfield/next.jdbc/1.1.582/doc/all-the-options#transactions
     (jdbc/with-transaction [txdatasource datasource
                             {:read-only true
                              :serializable true}]
       {:total (:count (jdbc/execute-one! txdatasource
                                          ["select count(id) from applications"]))
        :page (sql/query txdatasource
                         [(space-separated
                           ["select" (comma-separated
                                      applications-table-columns-except-resume)
                            "from" applications-table-name-str
                            "offset ? limit ?"])
                          offset limit])}))))


(defn get-resume
  "Given an application ID, return the corresponding resume bytes,
  and file name."
  ([id]
   (get-resume cdc/datasource id))
  ([datasource id]
   (first
    (sql/query datasource
               [(space-separated
                 ["select resume, resume_filename"
                  "from" applications-table-name-str
                  "where id = ?"])
                id]))))


(comment
  (let [mock-resume-bytes (byte-array (map byte "resume"))
        sample-application {:applicant_name (str "A Random Person " (rand))
                            :email "foobar@example.com"
                            :phone "+9198507654321"
                            :description "A random candidate added for no reason"
                            :resume mock-resume-bytes
                            :resume_filename "resume.pdf"}
        application (add-application sample-application)]
    [application
     (get-one-application application)
     (get-resume application)
     (update-application (assoc {}
                                :id application
                                :email (str "foobar" (rand) "@example.com")
                                :phone "+911111100000"))])

  (get-resume
   (java.util.UUID/randomUUID))
  )
