(ns cats.core
  (:gen-class)
  (:require [cats.db.migrations :as migrations]
            [cats.server :as server]
            [mount.core :as mount]))


(defn -main
  "Starts the system, configured based on the CATS_ENV env var, and with
  in-process database migrations.

  STARTUP:
  The startup sequence tries to avoid serving requests if migration fails.
  Otherwise, we could potentially fail to serve requests due to bad reads
  and/or corrupt the database with bad writes. So, we:
  1. Initialize: Start everything except the server.
  2. Migrate: Attempt in-process migrations.
  3. Serve: Start the server if migrations succeed.

  SHUTDOWN:
  We try to attempt safe shutdown, by:
  1. Requesting safe-shutdown of the non-daemon thread pools that
     back futures and agents.
  2. Using mount to stop all the other states in the proper order.

  We try to approximate the safe shutdown strategy outlined here:
  ref: https://medium.com/helpshift-engineering/achieving-graceful-restarts-of-clojure-services-b3a3b9c1d60d"
  [& args]
  ;; Initialize!
  (println "Initialize the system but don't serve requests yet.")
  (-> (mount/except [#'server/server])
      (mount/start))

  ;; Migrate!
  (println "Migrate the db!")
  (migrations/migrate!)

  ;; Serve!
  (println "Start jetty, and serve!")
  (mount/start [#'server/server])

  ;; Register safe-shutdown hook
  (.addShutdownHook
   ^java.lang.Runtime (Runtime/getRuntime)
   (Thread. ^Runnable (fn safe-shutdown-main []
                        (println "Request futures pool to stop.")
                        (shutdown-agents)
                        (println "Completely dismount the system.")
                        (mount/stop)))))
