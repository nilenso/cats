(defproject cats "0.1.0-SNAPSHOT"
  :description "What does a Clojurish Applicants Tracking Software look like?"
  :url "https://gitlab.com/nilenso/cats"
  :license {:name "EPL-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :min-lein-version "2.9.2" ; prefer Clojure 1.10.1
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/clojurescript "1.10.758"]

                 ;; http
                 [ring "1.8.1"]         ; github.com/ring-clojure/ring
                 [ring/ring-defaults "0.3.2"]
                 [bidi "2.1.6"]         ; github.com/juxt/bidi

                 ;; templates and data
                 [ring/ring-json "0.5.0"]
                 [hiccup "1.0.5"]       ; github.com/weavejester/hiccup
                 [cheshire "5.10.0"]    ; github.com/dakrone/cheshire

                 ;; databases
                 [seancorfield/next.jdbc "1.0.462"]
                 [org.postgresql/postgresql "42.2.14"]
                 [com.zaxxer/HikariCP "3.4.5"] ; https://github.com/brettwooldridge/HikariCP
                 [migratus "1.2.8"] ; migrations

                 ;; server
                 [mount "0.1.16"]

                 ;; configuration (db creds)
                 [aero "1.1.6"]

                 ;; TODO: Logging. Which one?
                 ]

  :profiles {:uberjar {:aot :all
                       :uberjar-exclusions [#"cljs"]}
             :dev {:source-paths ["dev"]
                   :resource-paths ["resources-devtest"]
                   :test-paths ["test/clj" "test/cljc" "test/cljs"]
                   :test-selectors {;; see README_DESIGN.org for notes on test selectors
                                    :default (fn [m]
                                               (not (clojure.string/starts-with?
                                                     (str (:ns m))
                                                     "cats.testutils")))}
                   :dependencies [[ring-server "0.5.0"]]
                   :plugins [[lein-ring "0.12.5"]
                             [migratus-lein "0.7.3"]]
                   :ring {:handler cats.routes/app
                          :init cats.user/init-system-ring-server
                          :destroy cats.user/destroy-system-ring-server}}
             :gitlab-ci {:local-repo ".local-m2"}}

  :source-paths ["src/clj" "src/cljc" "src/cljs"]
  :repl-options {:init-ns cats.user}
  :main cats.core)
