* Usage
** Purpose
   This org file is a "detangle" target for org-mode, which in turn is used
   to run tests and view results interactively with C-c C-c.

   How? Let's take an example:
   - We use bin/curl-tests.sh to exercise CATS APIs using curl
   - One of (empty) source blocks below tangles with that script
   - We open that script and with point in the script, `M-x org-babel-detangle`
   - This punches the script into the source block below
   - Now with point in the source block, we can run the tests with C-c C-c.
   - The results are output into the output file set in the source block header
   - If we open the output file side-by-side with the org-file, we can C-C C-c
     to interactively view results, because emacs helpfully auto-reloads the
     updated results output file.
** For local Git convenience
   We can't gitignore this file. So, if one wants to avoid accidental commits,
   and avoid having to checkout the file often, one can tell git to not track
   any local changes. (Smart idea from https://stackoverflow.com/a/11366713).
   #+begin_src shell
     git update-index --assume-unchanged README_TESTS.org
   #+end_src
   When we want to add something to it, like a new target code block, unset the
   no-track bit,
   #+begin_src shell
     git update-index --no-assume-unchanged README_TESTS.org
   #+end_src
   then commit changes, and then set it back to, once again, stop tracking.
   Further, to lookup which files are --assume-unchanged, use:
   #+begin_src shell
      git ls-files -v | grep -E "^[[:lower:]]
   #+end_src
   ref:
   https://git-scm.com/docs/git-update-index
   https://git-scm.com/docs/git-ls-files

* Curl tests
  #+NAME: curl-tests
  #+BEGIN_SRC shell :padline yes :tangle bin/curl-tests.sh :comments link :shebang #!/usr/bin/env bash :results value file :file test_curl_output :output-dir /tmp/
  #+END_SRC

  #+RESULTS:
  [[file:/tmp/test_curl_output]]
