#!/usr/bin/env bash

# Our server has 1GB memory, and on that machine, the jvm says that the
# default heap allocation would be about 16MB intial, and 256MB max:
# $ java -XX:+PrintCommandLineFlags 2> /dev/null
# -XX:InitialHeapSize=16078272 -XX:MaxHeapSize=257252352
#
# However we will run nginx and PG on the same box. And we're storing
# resumes in PG for now. So it may make sense to restrict the app's heap
# usage and give PG more headroom.
#
# Punting any decision till later, and just going with what jvm suggests.

declare -r cats_env="${CATS_ENV:?Fail. Environment not set.}"
declare -r cats_home="${CATS_HOME_DIR:?Fail. Home directory not set.}"

printf "%s\n" "INFO: About to start the cats jar with CATS_ENV=${cats_env}."
java -jar "${cats_home}/jars/latest.jar" \
     -Xms256m -Xmx256m
