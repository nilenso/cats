#!/usr/bin/env bash

prn_msg() {
    printf "INFO: %s\n" "$@"
}

fail_msg_bad_env() {
    printf "%s\n" "Fail. Var not set in CI build env."
}

ensure_valid_deploy_env() {
    local deploy_env="${CATS_ENV_DEPLOYS:?$(fail_msg_bad_env)}"
    prn_msg "About to attempt deployment for deploy env: $CATS_ENV_DEPLOYS."
}

build_uberjar() {
    prn_msg "About to build uberjar"
    lein with-profile +gitlab-ci uberjar
}

project_version() {
    grep '(defproject' project.clj |
        tr -s ' ' |
        cut -d ' ' -f3 |
        tr -d \"
}

install_deps() {
    prn_msg "Ensure open-ssh client, rsync, curl, and jq are installed."
    if ! which ssh-agent rsync curl jq > /dev/null
    then apt-get update -y
         apt-get install openssh-client rsync curl jq -y
    fi
}

init_ssh_agent() {
    local pvt_key_file="${CATS_SSH_PVT_KEY_FILE:?$(fail_msg_bad_env)}"
    local pvt_key_pass="${CATS_SSH_PVT_KEY_PASSWORD:?$(fail_msg_bad_env)}"
    local pvt_askpass_cmd="$(pwd)/askpass_cmd.sh"

    # cat the script literally (no interpolation)
    cat > $pvt_askpass_cmd <<- 'EOF'
#!/usr/bin/env bash
printf "%s" ${CATS_SSH_PVT_KEY_PASSWORD}
EOF

    chmod 0600 "$pvt_key_file"
    chmod 0700 "$pvt_askpass_cmd"

    prn_msg "Start ssh agent and private keyfile to it"
    eval $(ssh-agent -s)
    # If there's a display, ssh-add will prompt for a password. We can remove
    # tty/display if we pipe the key to ssh-add _and also_ unset DISPLAY.
    # If DISPLAY is set, then ssh-add will try to open an X11 window, which
    # works on a laptop, but won't in the build job's container runtime.
    # ref. https://dzone.com/articles/manage-ssh-key-file-with-passphrase
    # and ref. ssh-add manpage
    cat "$pvt_key_file" | SSH_ASKPASS=$pvt_askpass_cmd DISPLAY= ssh-add -

    # output which file added
    ssh-add -l
    # cleanup askpass cmd
    rm "$pvt_askpass_cmd"
}

rsync_into_remote_usr_home() {
    local remote_user="${CATS_REMOTE_USER:?$(fail_msg_bad_env)}"
    local host_addr="${CATS_REMOTE_HOST_ADDR:?$(fail_msg_bad_env)}"

    local source_path="${1:?Fail. Source path not specified.}"
    local dest_path="${2:?Fail. Destination path not specified.}"

    rsync -az --progress \
          -e "ssh -q -o UserKnownHostsFile=$(pwd)/resources-ops/ssh/known_hosts" \
          "$source_path" \
          "${remote_user}@${host_addr}:${dest_path}"
}

ssh_cmd() {
    local remote_user="${CATS_REMOTE_USER:?$(fail_msg_bad_env)}"
    local host_addr="${CATS_REMOTE_HOST_ADDR:?$(fail_msg_bad_env)}"

    local remote_cmd="${1:?Fail. Please pass command string to execute at remote end.}"

    ssh -q -o UserKnownHostsFile="$(pwd)/resources-ops/ssh/known_hosts" \
        "${remote_user}@${host_addr}" \
        "${remote_cmd}"
}

install_built_artefacts() {
    local secrets_edn="${CATS_AERO_SECRETS_EDN:?$(fail_msg_bad_env)}"
    local dest_home="${CATS_REMOTE_USER_HOME:?$(fail_msg_bad_env)}"

    local uberjar_name="cats-$(project_version)-standalone.jar"
    local jars_dir="${dest_home}/jars"

    prn_msg "About to rsync secrets over to remote host"
    rsync_into_remote_usr_home "${secrets_edn}" "${dest_home}/config/secrets.edn"

    prn_msg "About to rsync uberjar over to remote host"
    rsync_into_remote_usr_home "$(pwd)/target/${uberjar_name}" "${jars_dir}/${uberjar_name}"

    prn_msg "About to symlink rsync'd uberjar, as latest.jar, at remote host."
    ssh_cmd "ln --force -s ${jars_dir}/${uberjar_name} ${jars_dir}/latest.jar"
}

restart_remote_service() {
    prn_msg "About to restart cats.service in env: ${CATS_ENV_DEPLOYS}"
    ssh_cmd "sudo systemctl restart cats && systemctl status cats"
}

curl_smoke_test() {
    # TODO: This is not really a failing test, yet. Ideally it should be a
    # failing test, so we can use it to gate deploy, by running it pre-deploy.
    # And again post-deploy so we know things were up and running before and
    # after the deploy.
    # TODO: make it so that we don't need to specify the port.
    local host_addr="${CATS_REMOTE_HOST_ADDR:?$(fail_msg_bad_env)}"
    ./bin/curl-tests.sh "80" "${host_addr}"
}

install_deps

prn_msg "About to perform pre-deploy smoke test."
curl_smoke_test

prn_msg "About to commence build and install steps."
ensure_valid_deploy_env &&
    init_ssh_agent &&
    build_uberjar &&
    install_built_artefacts

prn_msg "About to restart the cats service."
restart_remote_service

# This is not really a failing test, yet
prn_msg "Sleeping for 10 seconds, before performing post-deploy smoke test."
sleep 10
curl_smoke_test
