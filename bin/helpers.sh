#!/usr/bin/env bash

init_dbs() {
    createdb cats_dev
    createdb cats_test
}
