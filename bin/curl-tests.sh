#!/usr/bin/env bash
# [[file:../README_TESTS.org::curl-tests][curl-tests]]
# ####################################################################
# Quick and dirty curl tests for API routes.
# NOTE: Please DO NOT delete any [[file:...]] comment line from the script.
# ####################################################################

init_script_globals() {
    app_port="${1:-3001}" # dev port by default
    app_host="${2:-localhost}"
    cookiejar="/tmp/test_curl_cookiejar"
    tempfile_application_ids="resources-devtest/data/cats-test-application-ids.deleteme"
}

cleanup_tmp_files() {
    rm -f "${cookiejar}" ${tempfile_application_ids}
}

cleanup_records_from_db() {
    while read user_id
    do psql --echo-queries --quiet -d "cats_dev" \
            -c "DELETE from applications where id = $(printf "\'%s\'" ${user_id});"
    done
}

get_url() {
    local route="${1:?'Fail. Please pass route.'}"
    local hostport=${2:-"${app_host}:${app_port}"}

    curl -s -L -b "${cookiejar}" \
         -XGET "http://${hostport}/${route}"
}

get_application() {
    local hostport=${1:-"${app_host}:${app_port}"}

    while read application_id
    do get_url "applications/${application_id}" "${hostport}"
    done
}

get_resumes() {
    local hostport=${1:-"${app_host}:${app_port}"}

    while read application_id
    do get_url "applications/${application_id}/resume" "${hostport}"
    done
}


post_urlencoded_form() {
    local route=${1:?"Fail. Please pass route."}
    local data=${2:?"Fail. Please pass urlencoded data."}
    local hostport=${3:-"${app_host}:${app_port}"}

    curl -s -L -c "${cookiejar}" \
         -XPOST -H "Content-Type: application/x-www-form-urlencoded" \
         -d "${data}" "http://${hostport}/${route}"
}

post_one_application() {
    local filename=${1:-"resources-devtest/data/resume.pdf"}
    local route=${2:-"applications"}
    local hostport=${3:-"${app_host}:${app_port}"}
    local rand_id="${RANDOM}"

    curl -s -L -c "${cookiejar}" \
         -F applicant_name="Culrd Applicant ${rand_id}" \
         -F email="curldapp${rand_id}@example.com" \
         -F phone="9876543210" \
         -F description="Curl was used to add this \"Curld Application ${rand_id}\" person." \
         -F resume=@${filename} \
         "http://${hostport}/${route}"
}

put_urlencoded_form() {
    local route=${1:?"Fail. Please pass route."}
    local data=${2:?"Fail. Please pass urlencoded data."}
    local hostport=${3:-"${app_host}:${app_port}"}

    curl -s -L -c "${cookiejar}" \
         -XPUT -H "Content-Type: application/x-www-form-urlencoded" \
         -d "${data}" "http://${hostport}/${route}"
}

put_one_application() {
    local route=${1:-"applications"}
    local filename=${2:-"resources-devtest/data/resume.pdf"}
    local hostport=${3:-"${app_host}:${app_port}"}
    local rand_id="${RANDOM}"

    curl -s -L -c "${cookiejar}" \
         -XPUT \
         -F id="${id}" \
         -F applicant_name="Culrd Applicant ${rand_id} - UPDATED" \
         -F email="curldapp${rand_id}@example.com" \
         -F phone="9876543210" \
         -F description="Curl was used to UPDATE this \"Curld Application ${rand_id}\" person." \
         -F resume=@${filename} \
         "http://${hostport}/${route}"
}


delete_entity() {
    local route=${1:?"Fail. Please pass route."}
    local hostport=${2:-"${app_host}:${app_port}"}

    curl -s -L -b "${cookiejar}" \
         -XDELETE "http://${hostport}/${route}"
}


about_to_execute() {
    printf "\n\n===%s===\n" "${1}"
}

do_hiring_mgr_actions() {
    printf "===%s===\n" "About to perform various hiring manager actions."

    about_to_execute "POST New application record"
    __application_added=$(post_one_application)
    echo "${__application_added}" |
        jq -r '.id' |
        # append to file that we will use to cleanup data at the end
        tee -a ${tempfile_application_ids}

    about_to_execute "GET existing application record"
    __application_id=$(echo "${__application_added}" |
                           # extract "raw" text-formatted output,
                           # rather than json-formatted string
                           jq -r '."id"')
    get_url "applications/${__application_id}" | jq '.'

    about_to_execute "PUT into existing application record"
    put_one_application "applications/${__application_id}" \
                        "resources-devtest/data/resume.md" | jq '.'

    about_to_execute "POST applications with different file types, GET resume files, and compare hashes of uploaded with downloaded."
    for filename in $(ls ./resources-devtest/data/resume.*)
    do post_one_application ${filename} |
            jq -r '.id' |
            get_application |
            jq -r '."applications/id"' |
            # append to file that we will use to cleanup data at the end
            tee -a ${tempfile_application_ids} |
            get_resumes > "${filename}.downloaded"

       sha256sum "${filename}" "${filename}.downloaded"
       rm "${filename}.downloaded"
    done

    __jq_summarise_get_applications() {
        jq -c '{"total","offset", "limit", "previous", "next", page: ."page" | length}'
    }

    about_to_execute "GET several applications, with default paging."
    get_url "applications" | __jq_summarise_get_applications

    about_to_execute "GET several applications, with empty string for offset and limit."
    curl -s "http://${app_host}:${app_port}/applications?offset=&limit=" | __jq_summarise_get_applications

    about_to_execute "GET several applications, with only limit provided."
    curl -s "http://${app_host}:${app_port}/applications?limit=50" | __jq_summarise_get_applications

    about_to_execute "GET several applications, with only offset provided."
    curl -s "http://${app_host}:${app_port}/applications?offset=10" | __jq_summarise_get_applications

    about_to_execute "GET several applications, with both offset and limit provided."
    curl -s "http://${app_host}:${app_port}/applications?offset=10&limit=50" | __jq_summarise_get_applications

    about_to_execute "GET several applications, with out-of-bounds limit."
    curl -s "http://${app_host}:${app_port}/applications?limit=101"

    about_to_execute "GET several applications, with out-of-bounds offset."
    curl -s "http://${app_host}:${app_port}/applications?offset=100000"

}


init_script_globals "${1}" "${2}"
do_hiring_mgr_actions

printf "\n\n===%s===\n" "Finished running tests."

# Cleanups

# Delete data directly from the database IFF we are NOT testing against
# a live deployment target.
# TODO: In staging env, delete via an API call when it becomes available.
# TODO: Block all tests for prod env entirely.
if [[ -z $CATS_ENV_DEPLOYS ]]
then about_to_execute "DELETE records from dev environment db"
     cat "${tempfile_application_ids}" |
        cleanup_records_from_db
else about_to_execute "Tests added applications with the following IDs to the db, in ${CATS_ENV_DEPLOYS}"
     cat "${tempfile_application_ids}"
fi

cleanup_tmp_files

# rm "/tmp/test_curl_output" # delete if needed
# curl-tests ends here
