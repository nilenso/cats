(ns cats.db.models-test
  (:require [cats.db.core :as cdc]
            [cats.db.models :as sut]
            [cats.testutils.common :as ctcommon]
            [cats.testutils.fixtures :as fixtures]
            [cats.utils :as cu]
            [clojure.test :as t]
            [next.jdbc :as jdbc])
  (:import org.postgresql.util.PSQLException))

(t/use-fixtures :once
  (t/join-fixtures [fixtures/system-start-stop-without-jetty
                    fixtures/recreate-db-schema]))


(defn delete-all-applications!
  []
  (jdbc/execute-one! cdc/datasource
                     ["TRUNCATE applications"]))


(t/deftest add-application-to-db-invariants
  (t/testing "Exercise invariants that \"add application\" must obey."
    (t/is (sut/add-application
           (ctcommon/gen-sample-application))
          "Add application returns application ID, when successful.")
    (t/is (thrown-with-msg? PSQLException
                            #"violates check constraint \"resume_filename_iff_resume\""
                            (sut/add-application
                             (dissoc (ctcommon/gen-sample-application)
                                     :resume_filename)))
          "DB does not permit adding application with resume file, but missing resume filename.")
    (t/is (thrown-with-msg? PSQLException
                            #"violates check constraint \"resume_filename_iff_resume\""
                            (sut/add-application
                             (dissoc (ctcommon/gen-sample-application)
                                     :resume)))
          "DB does not permit adding application with resume filename, but missing resume file.")
    (t/is (sut/add-application
           (dissoc (ctcommon/gen-sample-application)
                   :resume :resume_filename))
          "DB permits adding application with no resume file, and no resume filename.")
    (t/is (thrown-with-msg? PSQLException
                            #"violates not-null constraint"
                            (sut/add-application {}))
          "DB disallows adding malformed application.")))


(t/deftest get-application-invariants
  (t/testing "Exercise invariants that get application must obey."
    ;; NOTE: we use `name` to un-namespace keys of the record
    (t/is (= (sort sut/applications-table-columns-except-resume)
             (sort (mapv name
                         (keys (-> (ctcommon/gen-sample-application)
                                   sut/add-application
                                   sut/get-one-application))))
             (sort (mapv name
                         (keys (-> (dissoc (ctcommon/gen-sample-application)
                                           :resume :resume_filename)
                                   sut/add-application
                                   sut/get-one-application)))))
          "Get application returns exactly the expected fields for valid applications.")
    (t/is (not (contains? (-> (ctcommon/gen-sample-application)
                              sut/add-application
                              sut/get-one-application)
                          :applications/resume))
          "Get application does not contain resume file.")
    (t/is (nil? (sut/get-one-application (java.util.UUID/randomUUID)))
          "Get application returns nil for non-existent application.")))


(t/deftest get-applications-invariants
  (t/testing "Exercise invariants that get applications must obey."
    (let [_ (delete-all-applications!)]
      (t/is (= {:total 0
                :page []}
               (sut/get-applications))
            "Get applications returns a valid map with zero or empty values, when the table is empty."))
    (let [_ (delete-all-applications!)]
      (t/is (= {:total 1
                :page (conj [] (sut/get-one-application
                                (sut/add-application
                                 (ctcommon/gen-sample-application))))}
               (sut/get-applications))
            "Get applications returns applications with the same structure and values as get one application.")
      (t/is (<= (count (:page (sut/get-applications)))
                (:min sut/limit-range))
            "Get applications returns all available applications, when the table contains fewer applications than the minimum expected query page size."))
    (let [max-limit (:max sut/limit-range)
          min-limit (:min sut/limit-range)
          ;; setup a clean base
          _ (delete-all-applications!)
          ;; setup data to validate
          max-limit+1 (inc max-limit)
          _ (dotimes [_ max-limit+1]
              (sut/add-application
               (ctcommon/gen-sample-application)))]
      (t/is (= {:total max-limit+1
                :page min-limit}
               (update-in (sut/get-applications)
                          [:page] count))
            "By default, get applications returns a :page of minimum page size.")
      (t/is (= {:total max-limit+1
                :page max-limit}
               (update-in (sut/get-applications 0 max-limit+1)
                          [:page] count))
            "Get applications returns a :page of at most maximum allowed page size.")
      (t/is (= {:total max-limit+1
                ;; exactly these many applications in the last page
                :page min-limit}
               (update-in (sut/get-applications (- max-limit+1
                                                   min-limit)
                                                max-limit)
                          [:page] count))
            "Get applications returns the last :page correctly.")
      (t/is (= {:total max-limit+1
                :page []}
               (sut/get-applications max-limit+1
                                     1))
            "Get applications returns valid :total, along with empty :page, when offset exceeds :total applications.")
      (t/is (= {:total max-limit+1
                :page min-limit}
               (update-in (sut/get-applications -1 min-limit)
                          [:page] count))
            "Get applications safely handles negative offset value, by defaulting to 0.")
      (t/is (= {:total max-limit+1
                :page min-limit}
               (update-in (sut/get-applications 0 -1)
                          [:page] count))
            "Get applications safely handles negative page size value, by defaulting to min page size"))))


(t/deftest get-resume-for-application
  (t/testing "Return the appropriate resume map for various valid application records."
    (let [generated-application (assoc (ctcommon/gen-sample-application)
                                       :resume (cu/read-file->bytearray
                                                "resources-devtest/data/resume.md"))]
      ;; coerce to vectors, as byte-array equality is undefined
      (t/is (= (vec (:resume generated-application))
               (vec (:applications/resume
                     (-> generated-application
                         sut/add-application
                         sut/get-resume))))
            "Resume document bytes round-trip correctly from file to DB and back."))
    (let [generated-application (ctcommon/gen-sample-application)]
      (t/is (= (:resume_filename generated-application)
               (:applications/resume_filename
                (-> generated-application
                    sut/add-application
                    sut/get-resume)))
            "Resume filename round-trips correctly from file to DB and back."))
    (t/is (= {:applications/resume nil, :applications/resume_filename nil}
             (-> (ctcommon/gen-sample-application)
                 (dissoc :resume :resume_filename)
                 sut/add-application
                 sut/get-resume))
          "Resume is nil and resume filename is nil for applications that don't have a resume.")
    (t/is (nil? (sut/get-resume (java.util.UUID/randomUUID)))
          "Get resume returns nil for a non-existent application.")))


(t/deftest update-application-no-ops
  (t/testing "Illegal updates change nothing."
    (t/is (nil? (sut/update-application
                 nil))
          "Do nothing when passed nil.")
    (t/is (nil? (sut/update-application
                 {:id (sut/add-application (ctcommon/gen-sample-application))
                  :created_at (java.time.LocalDateTime/now)}))
          "Do nothing when passed a map with values that we don't update.")
    (t/is (nil? (sut/update-application
                 {:id (java.util.UUID/randomUUID)
                  :email (str "foobar" (rand) "@example.com")}))
          "Do nothing when passed an ID that does not exist in the system.")
    (t/is (thrown-with-msg? PSQLException
                            #"column \"bad_bad_field\" of relation \"applications\" does not exist"
                            (sut/update-application
                             {:id (java.util.UUID/randomUUID)
                              :bad_bad_field "WAT"}))
          "Fail on attempt to add fields that do not exist in the DB.")
    (let [original-application (sut/get-one-application
                                (sut/add-application
                                 (ctcommon/gen-sample-application)))]
      (t/is (thrown-with-msg? PSQLException
                              #"ERROR:.*not-null constraint"
                              (sut/update-application
                                 {:id (:applications/id original-application)
                                  :email nil}))
            "Fail on attempt to update a non-nullable field."))))


(t/deftest update-application-revising-values
  (t/testing "Valid updates are accepted, and updated values are returned properly."
    (let [original-application (sut/get-one-application
                                (sut/add-application
                                 (ctcommon/gen-sample-application)))
          new-email (str "foobar" (rand) "@example.com")
          new-resume-filename "a-brand-new-resume.pdf"]
      (t/is (= (sut/update-application
                {:id (:applications/id original-application)
                 :email new-email
                 :resume_filename new-resume-filename})
               ;; compare with immediately-retrieved application
               (sut/get-one-application
                (:applications/id original-application)))
            "Get application succeeds immediately after update application, and both return the same result."))
    (t/is (= (sort sut/applications-table-columns-except-resume)
             (sort (mapv name
                         (keys (sut/update-application
                                {:id (sut/add-application
                                      (ctcommon/gen-sample-application))
                                 :resume (byte-array (map byte "Brand new resume!"))})))))
          "Update application returns exactly the expected fields for valid applications.")))


(t/deftest update-application-deleting-values
  (t/testing "Update application lets us delete values safely."
    (let [original-application (sut/get-one-application
                                (sut/add-application
                                 (ctcommon/gen-sample-application)))
          updated-application (sut/update-application
                               {:id (:applications/id original-application)
                                :created_at nil
                                :updated_at nil
                                :description nil
                                :resume nil
                                :resume_filename nil})]
      (t/is (:applications/created_at updated-application)
            "Update application does not allow delete of created_at.")
      (t/is (:applications/updated_at updated-application)
            "Update application does not allow delete of updated_at.")
      (t/is (= {:applications/description nil
                :applications/resume_filename nil}
               (select-keys updated-application
                            [:applications/description
                             :applications/resume_filename]))
            "Update application can delete application description, and resume filename, which are nullable.")
      (t/is (nil? (:applications/resume
                   (sut/get-resume
                    (:applications/resume updated-application))))
            "Update application lets us delete the resume file, which is nullable."))))
