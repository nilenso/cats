(ns cats.testutils.common
  (:require [cats.utils :as cu]))


(defn gen-sample-application
  "Return a map that we can use in application-related operations, via handlers,
  and/or models.

  NOTE: The :resume field is a dummy byte-array, rather than bytes read from an
  actual file. The DB doesn't care as long as it's a ByteArray. Ergo, most tests
  need not care either. In the few cases where it matters, we can just override
  :resume as needed. E.g. to check if files round-trip correctly from disk, to
  handler, to DB, and back."
  []
  (let [random-suffix (rand-int java.lang.Integer/MAX_VALUE)]
    {:applicant_name (str "A Random Person " random-suffix)
     :email (str "foobar-" random-suffix "@example.com")
     :phone "+9198507654321"
     :description "A random candidate added for no reason"
     :resume (byte-array (map byte "dummy resume"))
     :resume_filename (format "resume-%s.pdf" random-suffix)}))
