(ns cats.testutils.fixtures
  (:require [cats.config :as cc]
            [cats.db.migrations :as migrations]
            cats.server
            [migratus.core :as migratus]
            [mount.core :as mount]))

(defn system-start-stop-without-jetty
  "Once fixture to start the system with :test profile. Normally we won't
  need the jetty serve, and starting it is expensive, so don't start it."
  [f]
  ;; NOTE: always call mount/stop at the top. Otherwise test runs perform
  ;; unclean startup, which breaks REPL-driven workflows.
  ;; - If we are REPL'd in, we want to code against a dev db, but run tests
  ;;   against an isolated test db.
  ;; - If we omit the stop call then mount/start re-uses the existing db state.
  ;; - If that is originally set to the dev db, via the REPL, then tests run
  ;;   against dev's db and drop dev data (bad!).
  ;; - Finally, once tests finish, mount remains saddled with broken state,
  ;;   which it cannot fix, leading to further failure in the REPL.
  (mount/stop)
  (-> (mount/except [#'cats.server/server])
      (mount/swap {#'cats.config/profile :test})
      mount/start)
  (f)
  (mount/stop))


(defn recreate-db-schema
  "Once fixture"
  [f]
  (migratus/reset
   migrations/migratus-config)
  (f))
