(ns cats.handler-test
  (:require [cats.db.core :as cdc]
            [cats.db.models :as models]
            [cats.handler :as sut]
            [cats.testutils.common :as ctcommon]
            [cats.testutils.fixtures :as fixtures]
            [clojure.java.io :as jio]
            [clojure.test :as t]
            [next.jdbc :as jdbc]
            [ring.util.response :as rur]))

(t/use-fixtures :once
  (t/join-fixtures [fixtures/system-start-stop-without-jetty
                    fixtures/recreate-db-schema]))


(defn delete-all-applications!
  []
  (jdbc/execute-one! cdc/datasource
                     ["TRUNCATE applications"]))


(defn make-get-applications-url
  [offset limit]
  (format "applications?offset=%s&limit=%s" offset limit))


(t/deftest get-one-application
  (t/testing "Testing the get application handler."
    (let [application-uuid (models/add-application
                            (ctcommon/gen-sample-application))]
      (t/is (= (:body (sut/get-one-application
                       {:route-params {:id (str application-uuid)}}))
               (models/get-one-application
                application-uuid))
            "The handler fn's return value agrees with the model fn's return value."))
    (t/is (nil? (:body (sut/get-one-application
                        {:route-params {:id (str (java.util.UUID/randomUUID))}})))
          "We get an empty response for a non-existent application ID.")))


(t/deftest get-applications
  (let [max-limit (:max models/limit-range)
        min-limit (:min models/limit-range)
        last-page-offset (- max-limit min-limit)
        _ (delete-all-applications!)
        _ (dotimes [_ max-limit]
            (models/add-application
             (ctcommon/gen-sample-application)))]
    (t/testing "Testing 200 OK behaviours of the get applications handler."
      (t/is (= {:total max-limit
                :offset 0
                :limit min-limit
                :next (make-get-applications-url 10 10)
                :previous (make-get-applications-url 0 10)
                :page (:page (models/get-applications 0 min-limit))}
               (:body (sut/get-applications {:params {}})))
            "By default, get-applications returns applications equal to the minimum defined page limit.")
      (t/is (= (sut/get-applications {:params {}})
               (sut/get-applications {:params {:offset ""
                                               :limit ""}}))
            "Get applications permits empty strings in limit and offset, and treats them like default values.")
      (t/is (= {:total max-limit
                :offset last-page-offset
                :limit min-limit
                :next (make-get-applications-url last-page-offset min-limit)
                :previous (make-get-applications-url (- last-page-offset min-limit) min-limit)
                :page (:page (models/get-applications last-page-offset  min-limit))}
               (:body (sut/get-applications {:params {:offset (str last-page-offset)
                                                      :limit (str min-limit)}})))
            "On querying the last page, we get exactly the last page, and the :next URL becomes a pointer to the last page itself.")
      (t/is (= {:total max-limit
                :offset (dec max-limit)
                :limit min-limit
                :next (make-get-applications-url last-page-offset min-limit)
                :previous (make-get-applications-url (- last-page-offset min-limit) min-limit)
                :page (:page (models/get-applications (dec max-limit) min-limit))}
               (:body (sut/get-applications {:params {:offset (str (dec max-limit))
                                                      :limit (str min-limit)}})))
            "On querying from offset near the end of the last page, we get back only the last few records. However, the :next url points to the entire last page, and the :previous url points to the entire previous page."))

    (t/testing "Testing 4XX behaviours of the get applications handler."
      (t/is (= 400
               (:status (sut/get-applications
                         {:params {:offset "-1"}})))
            "Get applications disallows negative offset.")
      (t/is (= 400
               (:status (sut/get-applications
                         {:params {:limit (str (dec min-limit))}}))
               (:status (sut/get-applications
                         {:params {:limit (str (inc max-limit))}})))
            "Get applications disallows :limit that is under/over the pre-defined maximum value.")
      (t/is (= 404
               (:status (sut/get-applications
                         {:params {:offset (str max-limit)}})))
            "Get application 404s if the client tries to offset beyond the last page."))))


(t/deftest add-application
  (t/testing "Testing the add application handler."
    (t/is (uuid? (:id (:body
                       (sut/add-one-application
                        {:params (-> (ctcommon/gen-sample-application)
                                     (select-keys [:applicant_name :email
                                                   :phone :description])
                                     (assoc :resume
                                            {:tempfile "resources-devtest/data/resume.pdf"
                                             :filename "resume.pdf"}))}))))
          "We get back the application's UUID when it is added successfully. We added it WITH resume.")
    (t/is (uuid? (:id (:body
                       (sut/add-one-application
                        {:params (-> (ctcommon/gen-sample-application)
                                     (select-keys [:applicant_name :email
                                                   :phone :description])
                                     (dissoc :resume))}))))
          "We get back the application's UUID when it is added successfully. We added it WITHOUT resume.")))


(t/deftest update-application
  (t/testing "Testing the update application handler."
    (let [sample-application (ctcommon/gen-sample-application)
          application-uuid (str (models/add-application sample-application))]
      (t/is (= (sut/update-one-application
                {:params (-> sample-application
                             (select-keys [:applicant_name :email
                                           :phone :description])
                             (assoc :resume
                                    {:tempfile "resources-devtest/data/resume.md"
                                     :filename "resume.md"}))
                 :route-params {:id application-uuid}})
               (sut/get-one-application
                {:route-params {:id (str application-uuid)}}))
            "Update application to overwrite fields INCLUDING the resume. The returned payload matches get application's payload."))
    (let [sample-application (ctcommon/gen-sample-application)
          application-uuid (str (models/add-application sample-application))]
      (t/is (= (sut/update-one-application
                {:params (-> sample-application
                             (select-keys [:applicant_name :email
                                           :phone :description])
                             (dissoc :resume))
                 :route-params {:id application-uuid}})
               (sut/get-one-application
                {:route-params {:id (str application-uuid)}}))
            "Update application to overwrite fields EXCEPT the resume. The returned payload matches get application's payload."))
    (t/is (= 400
             (:status (sut/update-one-application
                       {})))
          "Update resume 400s when we don't pass any data.")
    (t/is (= 400
             (:status (sut/update-one-application
                       {:params {:applicant_name nil
                                 :email nil}
                        :route-params {:id (str (models/add-application
                                                 (ctcommon/gen-sample-application)))}})))
          "Update resume 400s if try to perform disallowed updates, such as deleting name or email.")
    (t/is (= 400
             (:status (sut/update-one-application
                       {:route-params {:id (str (java.util.UUID/randomUUID))}})))
          "Update resume 400s when we pass data for application that does not exist.")))


(t/deftest get-resume
  (t/testing "Testing the get resume handler."
    (let [sample-application (ctcommon/gen-sample-application)
          application-id (str (models/add-application sample-application))]
      ;; coerce to vectors, as byte-array equality is undefined
      (t/is (= (vec (:resume sample-application))
               (vec (with-open [in (:body (sut/get-resume
                                           {:route-params {:id application-id}}))
                                out (java.io.ByteArrayOutputStream.)]
                      (jio/copy in out)
                      (.toByteArray out))))
            "Get resume response body has exactly the bytes we saved."))
    (let [sample-application (ctcommon/gen-sample-application)]
      (t/is (= (format "inline; filename=%s"
                       (:resume_filename sample-application))
               (rur/get-header (sut/get-resume
                                {:route-params
                                 {:id (str (models/add-application
                                            sample-application))}})
                               "Content-Disposition"))
            "Get resume response has the Content Disposition header set correctly."))
    (t/is (= 404
             (:status (sut/get-resume
                       {:route-params
                        {:id (str (models/add-application
                                   (dissoc (ctcommon/gen-sample-application)
                                           :resume :resume_filename)))}})))
          "Get resume 404s if we request resume for an application that doesn't contain it.")
    (t/is (= 404
             (:status (sut/get-resume
                       {:route-params
                        {:id (str (java.util.UUID/randomUUID))}})))
          "Get resume 404s if we request resume for a non-existent application.")))
