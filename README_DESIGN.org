* Domain Entities
** Application
   Don't model a "Candidate", because semantically it means a person.
   A person could apply multiple times, making unique ID error-prone.
   Consider "application". This way we can show matching searches to
   cover re-applications from the same "candidate", but with different
   contact info (new email ID, phone etc.)
** Candidates
*** Fields
    People who want jobs email us resumes. Relevant candidate context includes:
    - Name
    - Gender Male/Female/Nonbinary
    - CV (link to object store)
      - What if we accept Video interviews in the future?
    - Short "description" of the candidate. Use this to record essential context,
      about the candidate in general.
    - Communication channels we talk to them with:
      - Email
      - Chat (Slack)
      - Phone
      - In person
    - Meta-data
*** Binary data: where to put resumes?
    At the cost of maybe slow-ish queries, relatively slowish DB backup/restore, and possible future migration, one can avoid depending on a separate object store and worrying about consistency/weak transaction guarantees over the network. The security concerns are similar in either case (documents must be verified sane before ingesting).

    As far as I can tell, PG won't break a sweat for our use case. Our overall data set is likely to be small (a few ten GB or thereabouts). Even assuming this optimistic math, it's well within a single PG node's capability:
    #+BEGIN_SRC elisp
      (* 10.0 ; MB per candidate, all considered (resume, email, metadata, plaintext notes)
         100000 ; candidates, ever, assuming 1:1,000 :: hired:applied
         (/ 1.0 1024) ; into GB
         5            ; multiplier to account for PG's on-disk storage overhead as per the Wiki
         )
    #+END_SRC

    #+RESULTS:
    : 4882.8125
* Gitlab CI/CD
** Prep staging machine
   - Install postgresql, set up the db, and db user
   - Set up CATS app user, group, and its home dir
   - Systemd to start/stop the "cats" service
     - start a "latest" jar with "staging" profile
     - Allow the 'cats' user to restart the cats jar systemd service without sudo. Ref: https://unix.stackexchange.com/questions/192706/how-could-we-allow-non-root-users-to-control-a-systemd-service
   - Create SSH keys for the gitlab deploy user, set in gitlab, and add to the
     remote server
   - Make sure the cats service depends on postgresql. If PG isn't up, don't start at all.
   - set up nginx with conf for cats
   - remember to block the jetty server port (e.g. with ufw)
   - and don't forget to permit ssh, otherwise builds will fail
     #+BEGIN_SRC shell
     sudo ufw enable
     sudo ufw deny 3000
     sudo ufw app info 'Nginx Full'
     sudo ufw allow 'Nginx Full'
     sudo ufw allow ssh
     #+END_SRC
** Summary of their scheme:
   Gitlab drives CI/CD off a '.gitlab-ci.yaml' file in a repo's root, written
   per their [[https://gitlab.com/help/ci/yaml/README.md][yaml syntax reference]].

   The yaml file declares "jobs". A job is an arbitrary top-level key. Each job
   is run by a "runner".  A "runner" can be custom installed on infrastructure
   we control, or we can use "shared runners" provided by gitlab.
** Deps
   - Fetch lein deps, and cache .m2, and restore from cache each time
** Testing
   - Fail if compile fails
   - Then fail if tests fail
   - Punt linting
** Building
   - Create uberjar with leiningen
** Deploying
   - Set profile key, based on which environment we are in
   - Generate secrets.edn from heredoc, with secrets punched in, and copy over to the server
   - Set CATS_HOME_DIR on remote server to /home/cats/app/ (with the trailing slash)
   - Copy versioned uberjar
   - Symlink as "latest" (systemd will start this jar)
   - Stop system
   - Run migrations over ssh?
   - Start system
* Meta-learning
** Ring
   The core idea is just function composition with higher-order fns.
   https://www.slideshare.net/mmcgrana/one-ring-to-bind-them
   #+BEGIN_SRC clojure
     ;; If f, g, h are HoFs that behave like so:
     (let [a (-> f
                 g
                 h)]
       (a {...}))

     ;; an "app" is just a series of function transforms of a hash-map
     ;; the hash-map is request context, which is managed automatically
     ;; because it gets captured in every successive function's lexical scope
     (let [app (-> handler
                   wrap-request-logging
                   wrap-params
                   wrap-bounce-favicon)] ;; etc...
       (app incoming-request-map))
   #+END_SRC

   The "Ring" diagram illustrates and what each stage of the "ring" does.
   It's called "ring", because the author is a fan of Lord of The Rings,
   and figured the data flow looks like it's going in a ring, forever.
   https://www.slideshare.net/mmcgrana/ring-web-apps-in-idiomatic-clojure

   | /-------------------------> | CLIENT-->  | \                            |
   | ^                           |            | .                            |
   | .                           |            | v                            |
   | HTTP RESPONSE (to client)   |            | HTTP REQUEST (from client)   |
   | ^                           |            | .                            |
   | .                           |            | .                            |
   | .                           |            | v                            |
   | ADAPTER                     |            | ADAPTER                      |
   | ^ e.g. Clj->Jetty adapter   |            | . e.g. Jetty->Clj adapter    |
   | .                           |            | .                            |
   | .                           |            | v                            |
   | RING RESPONSE               |            | RING REQUEST                 |
   | ^ enriched output map       |            | . basic incoming map         |
   | . {:status 200              |            | . {:method :get, :uri "",    |
   | .  :headers {}              |            | .  :params ""                |
   | .  :body ""}                |            | .  :session ""}              |
   | .                           |            | .                            |
   | .                           |            | v                            |
   | MIDDLEWARE                  |            | MIDDLEWARE                   |
   | . e.g. wrap-json, wrap-file |            | . e.g. wrap-multipart-params |
   | . (fn [handler]             |            | . (fn [handler]              |
   | . . .(fn [response]         |            | . . .(fn [request]           |
   | . . . .(handler response))) |            | . . . .(handler request)))   |
   | .                           |            | .                            |
   | .                           |            | v                            |
   | RING RESPONSE               |            | RING REQUEST                 |
   | ^ a basic response map      |            | . an enriched hash-map       |
   | .                           |            | /                            |
   | \ <------------------------ | <--HANDLER |                              |

** PostgreSQL
*** What are the storage limits?
    IMHO, "[[https://wiki.postgresql.org/wiki/FAQ#What_is_the_maximum_size_for_a_row.2C_a_table.2C_and_a_database.3F][Practically unlimited for all but the most demanding applications]]."
    - Maximum size for a database? unlimited (32 TB databases exist)
    - Maximum size for a table? 32 TB
    - Maximum size for a row? 400 GB
    - Maximum size for a field? 1 GB
    - Maximum number of rows in a table? unlimited
    - Maximum number of columns in a table? 250-1600 depending on column types
    - Maximum number of indexes on a table? unlimited
*** To BLOB or not to BLOB?
    When is it OK to store binary data in your RDBMS, and when is it not?
    - [[https://www.microsoft.com/en-us/research/wp-content/uploads/2006/04/tr-2006-45.pdf][(PDF) To BLOB or Not To BLOB: Large Object Storage in a Database or a Filesystem?]]
    - [[https://wiki.postgresql.org/wiki/BinaryFilesInDB][PG Wiki: Binary Files in DB]]
    - https://www.cybertec-postgresql.com/en/binary-data-performance-in-postgresql/
    - https://momjian.us/main/blogs/pgblog/2020.html
    - [[https://www.postgresql.org/message-id/flat/CAC6Op19%3DNG9C61RqwDshhSZkm66Tsp%2BAyK-a5wukY%2B7ApbeDig%40mail.gmail.com#00735b4f6d7d1b65daf4572c8bb36223][pgsql-general mailing list thread]]
    - [[https://dba.stackexchange.com/a/226464][SO user's answer to "Should binary files be stored in the database?"]]
      The worst case scenario when you put a file in the database is very bad for performance, and compatibility with tooling. It's always exceptionally implementation dependent. In no way is the database better at being a file system then the file system. In every way, it's a compromise and the even when you get powerful mitigating features (like the case of SecureFile), the tooling is so poor that it's really not much more than a marketing point unless your whole stack is built by the RDBMS provider.

      Keep it simple, and the general rule is keep the files out of the DB.

      Exceptions: Storing files in the database has a few valid use cases,
      - When you need to edit the file transitionally. That means it's literally part of your transaction to edit the file. Or you need the ability to roll-back edits on the file if the transaction fails for data-integrity issues in the relations (tables).
      - When you need to ensure the file system is precisely versioned with the data and you can't afford any risk in keeping them in sync.
      - When you the database can actually parse the file and you can query it. In PostgreSQL for example, topologies can be queries with PostGIS. At this point, while it's a file it's also data for the query and not a storage dump.
*** How To Uniquely Identify a thing? UUID/GUID v/s the BigInteger world
    - Excellent discussion on [[https://groups.google.com/forum/?hl=en#!msg/microsoft.public.sqlserver.programming/qtCRhLLM9Kk/tg9vDfjbYW0J][Identity Vs. Uniqueidentifier (Newbie question)]] (2001, MS SQL server forum)
    - [[https://web.archive.org/web/20150511162734/http://databases.aspfaq.com/database/what-should-i-choose-for-my-primary-key.html][What should I choose for my primary key?]]
*** Videos: PG Capabilities, Optimisation
    - [[https://www.youtube.com/watch?v=HWfxUvW1ejw][PostgresOpen 2019 I Didn't Know Postgres Could Do That]]
    - [[https://www.youtube.com/watch?v=yhOkob2PQFQ][Postgres Open 2016 - Identifying Slow Queries and Fixing Them!]]
    - [[https://www.youtube.com/watch?v=t8-BQjWJFKw][PostgreSQL is the new NoSQL by Quentin Adam]]
    - [[https://www.youtube.com/watch?v=5M2FFbVeLSs][PostgreSQL performance in 5 minutes]]
    - [[https://www.youtube.com/watch?v=xrMbzHdPLKM][Tuning PostgreSQL for High Write Workloads]]
    - [[https://www.youtube.com/watch?v=8tnF1qqnagY][PostgresOpen 2019 Optimizing Query Performance from a Web Developer's Perspective]]
** Leiningen
*** Test Selectors
    We want to write utilities for tests, but we want to exclude them from the
    jars that we build. We want to keep test utility functions under the test/
    directory, to avoid polluting src/. Doing the latter would also require us
    to put source exclusions in the build job configuration, so those files do
    not get packaged into the jar. Putting such an exclusion in the build config.
    is undesirable as it depends on build tool support, i.e. it's not "automatic"
    because it doesn't follow standard java directory build conventions.

    However putting test utils under the test/ dir causes the lein test runner to
    pick up all the utility namespaces and run them even though they do not
    contain tests. This is annoying and can create mental overhead when debugging
    test failures... one always has to know which namespaces don't have tests, and
    consciously ignore them. Luckily, we can exclude these namespaces using
    leiningen's test selectors. The official leiningen docs are thin on how to
    use them well. Thankfully, the following blog posts provide good insights.
    - [[https://jakemccrary.com/blog/2019/01/28/how-to-use-leiningen-test-selectors-to-filter-by-test-name/][How to use Leiningen test selectors to filter by test name]]
    - [[https://medium.com/helpshift-engineering/the-convoluted-magic-of-leiningen-test-selectors-2eb6c452dfcf][The Convoluted Magic of Leiningen Test Selectors]]
