(ns cats.user
  (:require [cats.server :as server]
            [mount.core :as mount]))


(defn restart-dev-system!
  []
  (mount/stop)
  (mount/start))


(defn init-system-ring-server
  "Used to initialise ring-server, when running the app standalone,
   via the lein-ring plugin."
  []
  (println "CATS is starting.")
  (mount/start-without #'cats.server/server))


(defn destroy-system-ring-server
  "Used by ring-server in its shutdown hook, when running the app standalone."
  []
  (println "CATS is stopping.")
  (mount/stop))


(comment
  (restart-dev-system!)
  )
